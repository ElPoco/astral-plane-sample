This is a sample scene for astral plane support in Shadowrun Returns (DFDC)

It is meant for a mage/shaman PC0. Click on PC0 to have him/her astrally project. There's a mundane guard in the room to the South and a non-manifested spirit in the room to the East. Once projected, turns will alternate between astral and physical.

General concept

The global concept is to use the same map for both astral and physical plane, but swap the actors. The physical actors are replaced with auras (created in "Matrix" mode for that aura look) and vice-versa. Props can also be swapped if necessary. Some lightning changes and camera effects are added to make the astral plane look more astral. And many special rules and edge cases are handled to ensure that astral actions don't impact physical characters and vice-versa.

The scene

The scene has some special setting:
It has an "astral" dimension. Similar to the "Matrix" dimension, this let us have different round ratio between physical and astral and also to apply some triggers only in one dimension.
It has astral variants of each team. This is because actors from different dimensions will still attack each other if on different teams. Since we can have actors from both dimensions on the same map at the same time (astral bodies and auras, the body of a projected mage while in the astral plane, etc.) and we don't want physical actors to attack astral bodies not astral actors to attack auras, it's easier if each dimension has its own set of teams.

The map

The map is pretty much standard, except for two details:
 1. The doors are duplicated (one with the tag physical, the other with the tag astral)
 2. There is a holding closet with duplicates of actors. These are the auras/astral forms (they have the "aura" tag and the "astral body" tag for those that are linked to an astrally present characters). They are spawned with the "Matrix" spawn type, to look like auras. We could spawn them on demand but matching their appearance with the appearance of their physical form (same gear, etc.) would bit more complex. The auras are ambient actors immune to everything and with 0 speed to make sure they won't do anything and won't be affected by anything.

Another approach would have been to duplicate the map, update the position of each actor when necessary and teleport the camera between the "astral" map and the "physical" map. However, this makes the scene larger, forces the duplication of assets that don't really need it and forces the mapper to edit both maps for every change.

The triggers

The triggers are what makes this work, so let's study them one by one:

Init
When the map is loaded we:
- Set the speed of auras to Teleport, to mimick the concept that they move at the speed of thought. That's purely cosmetic.
- Detain all actors in the lonestar_astral team, since they're currently inactive.
- Hide all astral props.

InitPCAura
We need to link the "auras" actors to the physical actors. To do so, we tag each one with the id of the other. In order to know which aura to link with which character, we initally tag the aura with the physical actor's name.
We can't do this for the PC auras since we don't know their name beforehand. 
Also to ensure that the auras look like their physical counterpart, we just copy the actors and set the spawn type to "Matrix". We can't do that either for the PC characters. This trigger solves these two problems:
- It sets the prefab of the physical PC actors to the corresponding aura actors
- It tags the physical actor and aura actor with the other's id
- It also adapts the aura's name according to the PC name

ForceEndFirstPCTurn
Astral bodies are more complex than auras. They don't need to just be linked, they also need to have the other's stats (we'll see the details later). To do so, we update the aura's stats at the end of the first turn of its physical body. To make sure that the astral bodies are all setup correctly, we force the first player turn so that its astral body will be updated.

ForceEndFirstAuraTurn
We need PC0's astral body to have its first turn so that it gets its stats, but it shouldn't be able to do anything (or it might start blasting astral enemies in the holding closet), so we directly end the turn.

AfterPCAuraInit
So now that the PC astral body has been set-up, we disable the astral team's turns in the default dimension and we detain the astral actors, so that they don't act until we want them to. We also set the astral body to be player controlled, now that it's set-up (but it's detained so the player won't play it for now).

LinkAuras
This is the generic trigger to link non-PC actors with their auras. So when an actor with the tag physical start its turn and there's no alive actor with its id as a tag, we do the two-ways tag assignement of each other's ids.

ToggleAstral
A simple trigger to toggle astral (on if it's off, off if it's on)

ToggleAstralOn
We first add a "to_teleport" tag to all auras. 
We then teleport them to a random position in the holding closet. This serves only one purpose: to trigger the "SwapPhysicalAndAuras" trigger for each of the auras, which will let us do something for each one of them.
Then we set all physical props to invisible and all astral props to visible.
We add some camera effect, to make the astral more trippy.
We ensure that no physical actors can be hurt (most of them will be in the holding closet, but the body of astrally projected PC will still be shown), but that astral actors can be.
We set a "isAstral" variable to true so that we can check in some triggers if we're in the astral plane or not
We play a sound to underline the change to the astral
We set the light to 0, because in the astral everything that's not living is grey and unlit.
We free astral actors and detain physical ones.

ToggleAstralOff
More or less the same as above but in the other direction

SwapPhysicalAndAuras
The big one.
So in ToggleAstralOn we added a "to_teleport" tag to all auras and then we teleported them all. So this trigger will fire for each one of them. So for each aura we will:
- Retrieve the linked physical actor (the one that has the id of the aura as a tag) and we set its id to "temp_actor_id" to avoid having to retrieve it in every other lines.
- We enable manual turn-based mode, otherwise there are some side-effects when teleporting (I don't remember what exactly, but trust me on this one)
- We retrieve the current position and orientation of the linked phyiscal actor and store them in temp variables
- We run the "TeleportAwayOrNot" trigger. This trigger will teleport the linked physical actor to the holding closet, unless if it's astrally projecting (because these will stay shown even during the "physical" turn).
- Now that the room is clear for the aura to take the place where the physical actor used to be, we remove the "to_teleport" tag (so that we don't trigger this trigger again when we teleport the aura) and we teleport the aura to the positions we retrieved, in the right orientation. Since we have the orientation stored as an int but we can't use this int to set the orientation, we use it to build the name of the trigger that we will call. Each of the SwapTeleportX trigger will teleport the character to the point designated by the temp x and y variables and will set the orientation to the one given by the int value.
- Now that the teleport is done, we can disable manual turn-based mode.

RefreshAstralBodyStats
As mentionned above, we need to correctly set the stats of the astral bodies of astrally-projecting character. This trigger does this.

RefreshPhysicalBodyStats
If an astral body has been damaged, the physical body needs to be similarly damage. This trigger runs at the end of the astral turn of astral bodies to make sure the HP of the corresponding physical actor is the same. (It might not be necessary, since we have other triggers to handle this, but better safe than sorry.)

TransferDamage
Whenever an actor is damaged, its corresponding physical/astral body is also damaged

PhysicalDeath
If the physical actor dies, so does its aura

AstralDeath
If you die in the astral plane, you die in real-life. (We might be able to merge the two triggers as is done for the "TransferDamage" trigger), but we might want to have some specific handling in one case or another.

StashPCAstralStuff
The PC astral body needs to have the spells of its PC. The easiest way to enable this is to have the player do it in the equip screen. To make the player life easier, we stash all spells for him.

StashAuraAstralStuff
Same as the one before but in the other way

AstralStuffHelp
Display a help popup to explain why we show an equip screen

TransferPCStuff
- Display the help
- Stash the astral stuff
The equip screen will only show members of the Shadowrunner team. So we remove everyone from it (but we tag them as "shadowrunners" before so that we know who they are when we want to have them back on the Shadowrunner team) and we only add PC0 and its aura in the team. And then we show the equip screen.

RetransferPCStuff
Same as above, but in the other direction. Except that this time, we can just set the aura in the Shadowrunner team and have it stash everything it has to ensure that it's put in the Shadowrunner stash. However we don't open the equip screen just yet because it needs to be opened during the turn of the Shadowrunner team or the spells will not appear in PC0's inventory. That's why we just set a "isReturningFromAstral" variable to "true", so that we can trigger ReEquipPC0

ReEquipPC0
When PC0's turn starts, if the "isReturningFromAstral" variable is true, we open the equip screen so that it can re-requip its spells.

AfterStuffTransfer
The equip screen has been closed we restore the Shadowrunners to their team (and remove the tag so that we don't accidentally retransfer a character later that would have left the team later on). We also enable what we need for the physical or astral plane.

SwitchToAstral
This is the trigger that is called to astrally project PC0. 
We add the "astrally projected" tag to the physical body and the astral body, so that they won't teleport away when the other plane is shown.
We set PC0 to "downed" so that the physical body looks like it's not available for now.
We run ToggleAstral to setup the astral plane
We run TransferPCStuff to open the equip screen and do all the stuff explained above.

ReturnToBody
This is more or less the opposite of SwitchToAstral

AstralDoor1, AstralDoor2, PhysicalDoor1, PhysicalDoor2
Theoretically, astral forms can travel through walls but to make it easier for us we only let them pass through doors. So in the astral plane we'll display the doors with the "astral" tag and in the physical plane we'll display these with the "physical" tag. The former will teleport the aura behind them, the latter will open the door and will remove the corresponding astral door. 

PhysicalRoundStart and AstralRoundStart
When a round start in the Default dimension, we switch to the physical plane.
When a round starts in the Astral dimension and we have at least one projected PC, we switch to the astral plane.

